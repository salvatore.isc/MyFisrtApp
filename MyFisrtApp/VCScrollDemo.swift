//
//  VCScrollDemo.swift
//  MyFisrtApp
//
//  Created by Salvador Lopez on 08/06/23.
//

import UIKit

class VCScrollDemo: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: UIScrollView
        let rect = self.view.bounds
        let scrollView = UIScrollView(frame: rect)
        let img = UIImage(named: "colorful")
        let imgView = UIImageView(image: img)
        scrollView.addSubview(imgView)
        scrollView.contentSize = img!.size
        scrollView.contentInset = UIEdgeInsets(top: -60, left: 0, bottom: -60, right: 0)
        scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 20, left: 20, bottom: 10, right: 10)
        scrollView.contentOffset = CGPoint(x: 1200, y: 0)
        self.view.addSubview(scrollView)
        
    }

}
