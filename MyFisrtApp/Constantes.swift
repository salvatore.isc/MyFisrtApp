//
//  Constantes.swift
//  MyFisrtApp
//
//  Created by Salvador Lopez on 08/06/23.
//

import UIKit

struct Dimensiones{
    static let wScreen = UIScreen.main.bounds.width
    static let hScreen = UIScreen.main.bounds.height
}

