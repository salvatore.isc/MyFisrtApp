//
//  ViewController2.swift
//  MyFisrtApp
//
//  Created by Salvador Lopez on 07/06/23.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var textLabel: UILabel!
    
    //Switch
    @IBAction func updateValue(_ sender: UISwitch) {
        print(sender.isOn ? "Activado" : "Desactivado")
    }
    
    @IBAction func slider(_ sender: UISlider) {
        print(sender.value)
        textLabel.text = "$\(Int(sender.value))"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("[VC2] 1. La vista cargo.")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("[VC2] 2. La vista va a aparecer.")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("[VC2] 3. La vista apareció.")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("[VC2] 4. La vista va a desaparecer.")
    }

    override func viewDidDisappear(_ animated: Bool) {
        print("[VC2] 5. La vista desapareció.")
    }

    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
