//
//  ViewController.swift
//  MyFisrtApp
//
//  Created by Salvador Lopez on 05/06/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextFiled: UITextField!
    
    //MARK: UIView.animate()
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPass: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    
    @IBAction func btnAction(_ sender: Any) {
        print("Ouch!!")
    }
    
    @IBAction func btnAction2(_ sender: Any) {
        print(userNameTextField.text!)
        print(passwordTextFiled.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //Acceder a las propiedades
        myLabel.textColor = UIColor.black
        print("[VC] 1. La vista cargo.")
        
        //MARK: UIView.animate()
        self.view.backgroundColor = .white
        self.btnLogin.alpha = 0
        self.btnForgotPass.alpha = 0
        self.btnRegister.alpha = 0
        self.myLabel.alpha = 0
        self.imgView.frame.origin = CGPoint(x: -140, y: -140)
        self.imgView.alpha = 0
        self.userNameTextField.frame.origin.x = -300
        self.passwordTextFiled.frame.origin.x = -600
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("[VC] 2. La vista va a aparecer.")
        
        //MARK: UIView.animate()
        UIView.animate(withDuration: 2) {
            self.view.backgroundColor = .systemGray5
            self.imgView.alpha = 1
        }
        UIView.animate(withDuration: 1, delay: 0.5, animations: {
            self.imgView.frame.origin.x = 70
            self.imgView.frame.origin.y = 170
            self.userNameTextField.frame.origin.x = 90
            self.passwordTextFiled.frame.origin.x = 90
        }) { _ in
            UIView.animate(withDuration: 1) {
                self.myLabel.alpha = 1
                self.btnLogin.alpha = 1
                self.btnRegister.alpha = 1
                self.btnForgotPass.alpha = 1
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("[VC] 3. La vista apareció.")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("[VC] 4. La vista va a desaparecer.")
    }

    override func viewDidDisappear(_ animated: Bool) {
        print("[VC] 5. La vista desapareció.")
    }

}

