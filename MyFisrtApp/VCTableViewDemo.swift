//
//  VCTableViewDemo.swift
//  MyFisrtApp
//
//  Created by Salvador Lopez on 08/06/23.
//

import UIKit

class Cafe {
    var nombre: String
    var precio: Int = 10
    var promo: Bool = false
    
    init(_ nombre: String, precio: Int, promo: Bool){
        self.nombre = nombre
        self.precio = precio
        self.promo = promo
    }
}

class VCTableViewDemo: UIViewController {

    @IBOutlet weak var myTableView: UITableView!
    
    var cafes = [
        Cafe("Expresso", precio: 10, promo: false),
        Cafe("Expresso Doble", precio: 11, promo: true),
        Cafe("Expresso Cortado", precio: 12, promo: false),
        Cafe("Expresso con Crema", precio: 13, promo: true),
        Cafe("Chocolate", precio: 14, promo: false),
        Cafe("Chocolate Blanco", precio: 14, promo: false)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTableView.delegate = self
        myTableView.dataSource = self
    }

}

extension VCTableViewDemo: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cafes[indexPath.row].promo {
            print("\(cafes[indexPath.row].nombre) tiene una promoción 2x1, ¿Estas interesado?")
            
            //MARK: UIAlertController
            let alert = UIAlertController(title: "Promocion de la semana", message: "\(cafes[indexPath.row].nombre) tiene una promoción 2x1, ¿Estas interesado?", preferredStyle: .alert)
            
            let btn1 = UIAlertAction(title: "¡Si, estoy interesado!", style: .default){ _ in
                //TODO: El usuario esta interesado, agrega una bebida más sin costo
                print("El usuario esta interesado, agrega una bebida más sin costo")
            }
            
            let btn2 = UIAlertAction(title: "No por ahora.", style: .destructive){ _ in
                //TODO: Otra Accion...
            }
            
            alert.addTextField{ (texto) in
                texto.placeholder = "Ingresa tu codigo de promocion..."
            }
            
            alert.addAction(btn1)
            alert.addAction(btn2)
            
            self.present(alert, animated: true)
        }
    }
}

extension VCTableViewDemo: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cafes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        cell.textLabel?.text = cafes[indexPath.row].nombre
        cell.detailTextLabel?.text = String(cafes[indexPath.row].precio)
        if cafes[indexPath.row].promo {
            cell.backgroundColor = .yellow
        }
        return cell
    }
    
}
