//
//  VCLogin.swift
//  MyFisrtApp
//
//  Created by Salvador Lopez on 07/06/23.
//

import UIKit

class VCLogin: UIViewController {
    
    //Propiedades de la clase
    //Se accesible en todos elementos de la clase
    let label = UILabel()
    let mySwitch = UISwitch()
    let myActivityIndicator = UIActivityIndicatorView()
    
    //Variables UIProgressView
    var seconds : Float = 0
    var timer: Timer!
    var myProgressView: UIProgressView!

    override func viewDidLoad() {
        super.viewDidLoad()
        initUserInterface()
    }
    
    func initUserInterface(){
        
        //MARK: UILabel
        //let label = UILabel(frame: CGRect(x: 85, y: 50, width: 200, height: 50))
        label.frame = CGRect(x: (Dimensiones.wScreen/2)-100, y: 50, width: 200, height: 50)
        label.textAlignment = .center
        label.text = "Biemvenido!"
        label.layer.borderWidth = 1
        //214, 219, 223
        label.layer.borderColor = CGColor(red: 214/255, green: 219/255, blue: 223/255, alpha: 0.5)
        //self.view.addSubview(label)
        
        //MARK: UIButton
        let button = UIButton()
        button.frame = CGRect(x: (Dimensiones.wScreen/2)-100, y: 460, width: 200, height: 50)
        button.backgroundColor = UIColor.red
        button.setTitle("Login", for: .normal)
        button.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
        self.view.addSubview(button)
        
        //MARK: UITextField
        let userTextField = UITextField(frame: CGRect(x: (Dimensiones.wScreen/2)-100, y: 405, width: 200, height: 50))
        userTextField.textAlignment = .center
        userTextField.textColor = UIColor.blue
        //userTextField.borderStyle = .line
        userTextField.backgroundColor = .systemGray6
        userTextField.placeholder = "Ingresa tu usuario aqui..."
        userTextField.keyboardType = .emailAddress
        //self.view.addSubview(userTextField)
        
        //MARK: UIImageView
        let imageName = "GNU"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: (Dimensiones.wScreen/2)-100, y: 160, width: 200, height: 200)
        //image.contentMode = .scaleAspectFit
        //self.view.addSubview(imageView)
        
        //MARK: UITextView
        let str : NSString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ornare eget tellus eget tempus. Nam vel turpis sit amet ligula viverra laoreet. Sed consequat nunc id enim facilisis, ullamcorper varius eros dictum. Nulla id accumsan ex, vel blandit leo. Proin pretium venenatis purus, id finibus risus placerat non. Cras ac scelerisque ipsum. Integer porta fermentum magna, sed eleifend dolor lobortis non. Aliquam luctus fringilla pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi a condimentum erat. Vestibulum viverra est in urna fringilla, nec viverra massa vestibulum."
        let textView = UITextView(frame: CGRect(x: 15, y: 520, width: 350, height: 50))
        textView.text =  str as String
        textView.isEditable = false
        textView.isSelectable = false
        //self.view.addSubview(textView)
        
        //MARK: UITextView2
        let myBoldWord = str.range(of: "Lorem ipsum")
        
        let myMutableString = NSMutableAttributedString(string: str as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 18)!])
        
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Helvetica Neue", size: 36)!, range: myBoldWord)
        
        let textView2 = UITextView(frame: CGRect(x: 15, y: 590, width: 250, height: 50))
        textView2.attributedText = myMutableString
        
        //self.view.addSubview(textView2)
        
        //MARK: UISlider
        let mySlider = UISlider(frame: CGRect(x: 15, y: 680, width: 300, height: 50))
        mySlider.minimumValue = 0
        mySlider.maximumValue = 100
        mySlider.isContinuous = false
        mySlider.tintColor = UIColor.green
        mySlider.value = 10
        mySlider.addTarget(self, action: #selector(sliderUpdateValue(sender:)), for: .valueChanged)
        //self.view.addSubview(mySlider)
        
        //MARK: UISwitch
        mySwitch.frame = CGRect(x: 330, y: 680, width: 50, height: 50)
        mySwitch.isOn = false
        mySwitch.addTarget(self, action: #selector(switchUpdateValue(sender:)), for: .valueChanged)
        //self.view.addSubview(mySwitch)
        
        //MARK: UIActivityIndicator
        myActivityIndicator.style = .large
        myActivityIndicator.center = self.view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()
        self.view.addSubview(myActivityIndicator)
        
        //MARK: UIProgressView
        myProgressView = UIProgressView(progressViewStyle: .default)
        myProgressView.frame.origin = CGPoint(x: 95, y: 575)
        myProgressView.frame.size = CGSize(width: 200, height: 50)
        myProgressView.progress = 0
        myProgressView.progressTintColor = UIColor.red
        myProgressView.trackTintColor = UIColor.lightGray
        self.view.addSubview(myProgressView)
        //exTimer()
        
        //MARK: UIStepper
        let myStepper = UIStepper(frame: CGRect(x: 15, y: 530, width: 50, height: 50))
        myStepper.maximumValue = 10
        myStepper.minimumValue = 1
        myStepper.value = 5
        myStepper.autorepeat = true
        myStepper.wraps = true
        myStepper.addTarget(self, action: #selector(stepperUpdateValue(sender:)), for: .valueChanged)
        self.view.addSubview(myStepper)
        
        //MARK: UIDatePicker
        let myDatePicker = UIDatePicker(frame: CGRect(x: 15, y: 75, width: 0, height: 0))
        myDatePicker.datePickerMode = .dateAndTime
        myDatePicker.backgroundColor = .white
        //myDatePicker.preferredDatePickerStyle = .wheels
        myDatePicker.addTarget(self, action: #selector(datePickerUpdateValue(sender:)), for: .valueChanged)
        self.view.addSubview(myDatePicker)
        
        
    }
    
    @objc func datePickerUpdateValue(sender:UIDatePicker){
        print(sender.date)
        let dateFormater: DateFormatter = DateFormatter()
        dateFormater.dateFormat = "MM/dd/yyyy hh:mm:ss a zzz"
        let selectedDate = dateFormater.string(from: sender.date)
        print(selectedDate)
    }
    
    @objc func stepperUpdateValue(sender:UIStepper){
        print("Value: \(sender.value)")
    }
    
    @objc func loginAction(){
        //print("Hola")
        self.view.backgroundColor = .systemGray5
        self.label.text = "¡Bienvenido!"
        self.mySwitch.setOn(true, animated: true)
        self.myActivityIndicator.stopAnimating()
    }
    
    @objc func sliderUpdateValue(sender:UISlider){
        print(sender.value)
    }
    
    @objc func switchUpdateValue(sender:UISwitch){
        print(sender.isOn ? "ACTIVADO" : "DESACTIVADO")
    }
    
    func exTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateProgressView), userInfo: nil, repeats: true)
    }
    
    @objc func updateProgressView(){
        seconds += 1
        if seconds <= 10 {
            myProgressView.setProgress(seconds/10, animated: true)
            print("\(seconds * 10)%")
        }else{
            timer.invalidate()
        }
    }
    

}
