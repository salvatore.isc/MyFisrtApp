//
//  VCPickerView.swift
//  MyFisrtApp
//
//  Created by Salvador Lopez on 08/06/23.
//

import UIKit

class VCPickerView: UIViewController {
    
    var options = ["Blanco","Rojo","Verde","Azul","Amarillo"]
    var colors = [UIColor.white,UIColor.red,UIColor.green,UIColor.blue,UIColor.yellow]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: UIPickerView
        let myPickerView = UIPickerView()
        myPickerView.frame = CGRect(x: 15, y: 100, width: self.view.bounds.width - 15, height: 280)
        myPickerView.delegate = self
        myPickerView.dataSource = self
        myPickerView.backgroundColor = .white
        self.view.addSubview(myPickerView)
    }
    
}

extension VCPickerView: UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return options[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("Celda: \(row), Componente: \(component)")
        print(options[row])
        self.view.backgroundColor = colors[row]
    }
    
}

extension VCPickerView: UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return options.count
    }
    
}
